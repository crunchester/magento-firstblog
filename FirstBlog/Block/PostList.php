<?php
namespace Ivan\FirstBlog\Block;

use Ivan\FirstBlog\Api\Data\PostInterface;
use Ivan\FirstBlog\Api\PostRepositoryInterface;
use Ivan\FirstBlog\Model\PostRepository;
use Ivan\FirstBlog\Model\ResourceModel\Post\Collection as PostCollection;
use Ivan\FirstBlog\Model\ResourceModel\Post\CollectionFactory;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Api\SortOrderBuilder;

class PostList extends \Magento\Framework\View\Element\Template implements
    \Magento\Framework\DataObject\IdentityInterface
{
    /**
     * @var \Ivan\FirstBlog\Model\ResourceModel\Post\CollectionFactory
     */
    protected $_postCollectionFactory;
    /**
     * @var PostRepository
     */
    protected $postRepository;
    /**
     * @var SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;
    /**
     * @var SortOrderBuilder
     */
    private $sortOrderBuilder;
    /**
     * @var Collection
     */
    private $postCollectionFactory;

    /**
     * Construct
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Ivan\FirstBlog\Model\ResourceModel\Post\CollectionFactory $postCollectionFactory,
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        PostRepositoryInterface $postRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        SortOrderBuilder $sortOrderBuilder,
        CollectionFactory $postCollectionFactory,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->postRepository = $postRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->sortOrderBuilder = $sortOrderBuilder;
        $this->postCollectionFactory = $postCollectionFactory;
    }

    /**
     * @return \Ivan\FirstBlog\Model\ResourceModel\Post\Collection
     */
    public function getPosts()
    {
        // Check if posts has already been defined
        // makes our block nice and re-usable! We could
        // pass the 'posts' data to this block, with a collection
        // that has been filtered differently!
        $searchCriteria = $this->searchCriteriaBuilder->addFilter('is_active', 1)->create();
        if (!$this->hasData('posts')) {

            // Filter products weighing 10kg or more
            $this->searchCriteriaBuilder
                ->addFilter('is_active', 1);

            // Sort products heaviest to lightest
            $sortOrder = $this->sortOrderBuilder
                ->setField(PostInterface::CREATION_TIME)
                ->setDirection(SortOrder::SORT_DESC)
                ->create();

            $this->searchCriteriaBuilder->addSortOrder($sortOrder);

            // Create the SearchCriteria
            $searchCriteria = $this->searchCriteriaBuilder->create();

            // Load the products
            $posts = $this->postRepository
                ->getList($searchCriteria)->getItems();

                $collection = $this->postCollectionFactory->create($posts);

            $this->setData('posts', $collection);
        }
        return $this->getData('posts');
    }


    /**
     * Return identifiers for produced content
     *
     * @return array
     */
    public function getIdentities()
    {
        return [\Ivan\FirstBlog\Model\Post::CACHE_TAG . '_' . 'list'];
    }

    /**
     * @return $this|\Magento\Framework\View\Element\Template
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();

        if ($this->getPosts()) {
            $pager = $this->getLayout()->createBlock(
                'Magento\Theme\Block\Html\Pager',
                'fme.news.pager'
            )->setAvailableLimit(array(5=>5,10=>10,15=>15))->setShowPerPage(true)->setCollection(
                $this->getPosts()
            );
            $this->setChild('pager', $pager);
            $this->getPosts()->load();
        }
        return $this;
    }



    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }

}