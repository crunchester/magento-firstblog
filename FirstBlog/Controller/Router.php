<?php

namespace Ivan\FirstBlog\Controller;

class Router implements \Magento\Framework\App\RouterInterface
{
    /**
     * @var \Magento\Framework\App\ActionFactory
     */
    protected $actionFactory;

    /**
     * Post factory
     *
     * @var \Ivan\FirstBlog\Model\PostFactory
     */
    protected $_postFactory;
    /**
     * @var \Magento\Framework\Controller\Result\RedirectFactory
     */
    private $redirectFactory;

    /**
     * @param \Magento\Framework\App\ActionFactory $actionFactory
     * @param \Ivan\FirstBlog\Model\PostFactory $postFactory
     */
    public function __construct(
        \Magento\Framework\App\ActionFactory $actionFactory,
        \Ivan\FirstBlog\Model\PostFactory $postFactory,
        \Magento\Framework\Controller\Result\RedirectFactory $redirectFactory
    )
    {
        $this->actionFactory = $actionFactory;
        $this->_postFactory = $postFactory;
        $this->redirectFactory = $redirectFactory;
    }

    /**
     * Validate and Match FirstBlog Post and modify request
     *
     * @param \Magento\Framework\App\RequestInterface $request
     * @return bool
     */
    public function match(\Magento\Framework\App\RequestInterface $request)
    {

        if (!stristr($request->getPathInfo(), '/firstblog/'))
            return null;

        $url_key = str_replace('/firstblog/view/', '', $request->getPathInfo());

        $url_key = rtrim($url_key, '/');

        /** @var \Ivan\FirstBlog\Model\Post $post */
        $post = $this->_postFactory->create();

        $post_id = $post->checkUrlKey($url_key);
        if (!$post_id) {
            return null;
        }

        $request->setModuleName('firstblog')
            ->setControllerName('view')
            ->setActionName('index')
            ->setParam('post_id', $post_id);

//        die($request->getActionName());


        $request->setAlias(\Magento\Framework\Url::REWRITE_REQUEST_PATH_ALIAS, $url_key);

        return $this->actionFactory->create('Magento\Framework\App\Action\Forward');
    }
}