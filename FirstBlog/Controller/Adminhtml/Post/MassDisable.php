<?php
/**
 * Created by PhpStorm.
 * User: ivan
 * Date: 05.02.19
 * Time: 12:01
 */

namespace Ivan\FirstBlog\Controller\Adminhtml\Post;

use Ivan\FirstBlog\Model\PostRepository;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\RequestInterface;
use Magento\Ui\Component\MassAction\Filter;
use Ivan\FirstBlog\Model\ResourceModel\Post\CollectionFactory;
use Magento\Framework\Controller\ResultFactory;


class MassDisable extends \Magento\Backend\App\Action
{

    /**
     * @var Filter
     */
    protected $filter;

    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var RequestInterface
     */
    protected $request;

    /**
     * @var PostRepository
     */
    private $postRepository;

    /**
     * @param Context $context
     * @param Filter $filter
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(
        Context $context,
        Filter $filter,
        CollectionFactory $collectionFactory,
        PostRepository $postRepository,
        RequestInterface $request
    )
    {
        $this->filter = $filter;

        $this->collectionFactory = $collectionFactory;

        $this->request = $request;

        $this->postRepository = $postRepository;

        parent::__construct($context);

    }

    /**
     * Execute action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     * @throws \Magento\Framework\Exception\LocalizedException|\Exception
     */
    public function execute()
    {

        $collection = $this->filter->getCollection($this->collectionFactory->create());

        foreach ($collection as $item) {
            $item->setIsActive(false);
            $item->save();
        }

        $this->messageManager->addSuccess(__('A total of %1 record(s) have been disabled.', $collection->getSize()));

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('*/*/');
    }
}