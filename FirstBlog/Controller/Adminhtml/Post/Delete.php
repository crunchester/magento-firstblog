<?php
/**
 * Created by PhpStorm.
 * User: ivan
 * Date: 05.02.19
 * Time: 13:09
 */

namespace Ivan\FirstBlog\Controller\Adminhtml\Post;

use Ivan\FirstBlog\Model\PostRepository;
use Magento\Backend\App\Action;
use Magento\TestFramework\ErrorLog\Logger;

class Delete extends \Magento\Backend\App\Action
{
    /**
     * @var PostRepository
     */
    protected $postRepository;


    /**
     * Delete constructor.
     * @param Action\Context $context
     * @param PostRepository $postRepository
     */
    public function __construct(
        Action\Context $context,
        PostRepository $postRepository
)
    {

        parent::__construct($context);
        $this->postRepository = $postRepository;
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Ivan_FirstBlog::delete');
    }

    /**
     * Delete action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('post_id');
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($id) {
            try {
                $model = $this->postRepository->getById($id);
                $model->delete();

                $this->messageManager->addSuccess(__('The post has been deleted.'));
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
                return $resultRedirect->setPath('*/*/edit', ['post_id' => $id]);
            }
        }
        $this->messageManager->addError(__('We can\'t find a post to delete.'));
        return $resultRedirect->setPath('*/*/');
    }
}