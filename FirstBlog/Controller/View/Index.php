<?php
/**
 * Created by PhpStorm.
 * User: ivan
 * Date: 04.02.19
 * Time: 17:32
 */

namespace Ivan\FirstBlog\Controller\View;

use Magento\Framework\App\Action\Action;

class Index extends Action
{

    /** @var  \Magento\Framework\View\Result\Page */
    protected $resultPageFactory;

    /**
     * @var \Ivan\FirstBlog\Helper\Post
     */
    private $helper;

    /**
     * @param \Magento\Framework\App\Action\Context $context
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Controller\Result\ForwardFactory $resultForwardFactory,
        \Ivan\FirstBlog\Helper\Post $helper
    )
    {
        $this->resultForwardFactory = $resultForwardFactory;

        $this->helper = $helper;

        parent::__construct($context);

    }

    /**
     * Blog Index, shows a list of recent blog posts.
     *
     * @return \Magento\Framework\View\Result\PageFactory
     */
    public function execute()
    {
        $post_id = $this->getRequest()->getParam('post_id', $this->getRequest()->getParam('id', false));

        /** @var \Ivan\FirstBlog\Helper\Post $post_helper */
        $post_helper = $this->helper;

        $result_page = $post_helper->prepareResultPost($this, $post_id);

        if (!$result_page) {
            $resultForward = $this->resultForwardFactory->create();
            return $resultForward->forward('noroute');
        }

        return $result_page;
    }

}