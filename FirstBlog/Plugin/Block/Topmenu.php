<?php
/**
 * Created by PhpStorm.
 * User: ivan
 * Date: 07.02.19
 * Time: 11:52
 */

namespace Ivan\FirstBlog\Plugin\Block;


use Magento\Framework\Data\Tree\NodeFactory;
use Magento\Framework\UrlInterface;

class Topmenu
{

    /**
     * @var NodeFactory
     */
    protected $nodeFactory;

    /**
     * @var UrlInterface
     */
    protected $url;


    /**
     * Topmenu constructor.
     * @param NodeFactory $nodeFactory
     * @param Request $request
     */
    public function __construct(
        NodeFactory $nodeFactory,
        UrlInterface $url
    )
    {
        $this->nodeFactory = $nodeFactory;
        $this->url = $url;
    }

    /**
     * @param \Magento\Theme\Block\Html\Topmenu $subject
     * @param string $outermostClass
     * @param string $childrenWrapClass
     * @param int $limit
     */
    public function beforeGetHtml(
        \Magento\Theme\Block\Html\Topmenu $subject,
        $outermostClass = '',
        $childrenWrapClass = '',
        $limit = 0
    )
    {
        $node = $this->nodeFactory->create(
            [
                'data' => $this->getNodeAsArray(),
                'idField' => 'id',
                'tree' => $subject->getMenu()->getTree()
            ]
        );
        $subject->getMenu()->addChild($node);
    }

    /**
     * @return array
     */
    protected function getNodeAsArray()
    {
        return [
            'name' => __('Ivan Blog'),
            'id' => 'ivan_firstblog_link',
            'url' => '/firstblog/',
            'has_active' => false,
            'is_active' => $this->isActiveLink() // (expression to determine if menu item is selected or not)
        ];
    }

    /**
     * @return bool
     */
    private function isActiveLink()
    {
        return !stristr($this->url->getCurrentUrl(), '/firstblog/') ? false : true;
    }

}