<?php
/**
 * Created by PhpStorm.
 * User: ivan
 * Date: 06.02.19
 * Time: 09:57
 */

namespace Ivan\FirstBlog\Model;


use Braintree\Exception\NotFound;
use Ivan\FirstBlog\Api\Data\PostInterface;
use Ivan\FirstBlog\Api\PostRepositoryInterface;
use Ivan\FirstBlog\Model\ResourceModel\Post\CollectionFactory;
use Magento\Framework\Exception\NoSuchEntityException;

class PostRepository implements PostRepositoryInterface
{

    /**
     * @var
     */
    public $postFactory;

    /**
     * @var
     */
    public $postResource;

    /**
     * @var CollectionFactory
     */
    public $postCollectionFactory;

    /**
     * @var Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface
     */
    public $collectionProcessor;

    public $searchResultFactory;

    /**
     * PostRepository constructor.
     * @param \Magento\Catalog\Model\CategoryFactory $categoryFactory
     * @param \Magento\Catalog\Model\ResourceModel\Category $categoryResource
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(
        PostFactory $postFactory,
        \Ivan\FirstBlog\Model\ResourceModel\Post $postResource,
        CollectionFactory $postCollectionFactory,
        \Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface $collectionProcessor,
        \Magento\Cms\Api\Data\BlockSearchResultsInterfaceFactory $searchResultsFactory
    )
    {
        $this->postFactory = $postFactory;
        $this->postResource = $postResource;
        $this->postCollectionFactory = $postCollectionFactory;
        $this->collectionProcessor = $collectionProcessor;
        $this->searchResultsFactory = $searchResultsFactory;
    }

    /**
     * @param Data\PostInterface $post
     * @return mixed
     */
    public function save(\Ivan\FirstBlog\Api\Data\PostInterface $post)
    {
        try {
            $this->postResource->save($post);
        } catch (\Exception $e) {
            throw new CouldNotSaveException(
                __(
                    'Could not save category: %1',
                    $e->getMessage()
                ),
                $e
            );
        }

        return $this->getById($post->getId());
    }

    /**
     * @param $productId
     * @return Post
     */
    public function getById($postId)
    {
        $post = $this->postFactory->create();
        $post->load((int)$postId);

        if (!$post->getId()) {
            return null;
        }
        return $post;
    }

    /**
     * Delete product
     *
     * @param \Magento\Catalog\Api\Data\ProductInterface $product
     * @return bool Will returned True if deleted
     * @throws \Magento\Framework\Exception\StateException
     */
    public function delete(PostInterface $post)
    {
        try {
            $this->postResource->delete($post);
        } catch (\Exception $e) {
            throw new StateException(
                __(
                    'Cannot delete category with id %1',
                    $post->getId()
                ),
                $e
            );
        }
        return true;
    }


    /**
     * @param $productId
     * @return mixed
     */
    public function deleteById($postId)
    {
        $post = $this->getById($postId);

        $this->delete($post);
    }

    /**
     * Get product list
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Magento\Catalog\Api\Data\ProductSearchResultsInterface
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria)
    {
        /** @var \Magento\Cms\Model\ResourceModel\Block\Collection $collection */
        $collection = $this->postCollectionFactory->create();

        $this->collectionProcessor->process($searchCriteria, $collection);

        /** @var Data\BlockSearchResultsInterface $searchResults */
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);
        $searchResults->setItems($collection->getItems());
        $searchResults->setTotalCount($collection->getSize());

        return $searchResults;
    }

}