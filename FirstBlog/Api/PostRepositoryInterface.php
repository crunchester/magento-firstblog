<?php
/**
 * Created by PhpStorm.
 * User: ivan
 * Date: 06.02.19
 * Time: 09:48
 */

namespace Ivan\FirstBlog\Api;


interface PostRepositoryInterface
{
    /**
     * @param Data\PostInterface $post
     * @return mixed
     */
    public function save(\Ivan\FirstBlog\Api\Data\PostInterface $post);

    /**
     * @param $productId
     * @return mixed
     */
    public function getById($postId);

    /**
     * Delete product
     *
     * @param \Magento\Catalog\Api\Data\ProductInterface $product
     * @return bool Will returned True if deleted
     * @throws \Magento\Framework\Exception\StateException
     */
    public function delete(\Ivan\FirstBlog\Api\Data\PostInterface $post);

    /**
     * @param $productId
     * @return mixed
     */
    public function deleteById($postId);

    /**
     * Get product list
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Magento\Catalog\Api\Data\ProductSearchResultsInterface
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

}